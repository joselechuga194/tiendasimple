from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator
from .forms import *
from .models import Producto
from .filters import ProductoFilter
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required(login_url='cuenta:inicio_sesion')
def index(request):
    """
    Vista para el index de producto
    """
    return render(request, 'producto/index.html')


@login_required(login_url='cuenta:inicio_sesion')
def crearProducto(request):
    """
    Vista para crear un nuevo producto
    """
    template = 'producto/crear.html'

    if request.method == "GET":
        form = ProductoForm()
        context = {"formulario": form}

        # user = request.user
        # productos = Producto.objects.all()
        # for producto in productos:
        #     producto.creador = user
        #     producto.save()

        return render(request, template, context)

    if request.method == "POST":
        form = ProductoForm(request.POST)
        if form.is_valid():
            # Para asignar el creador guardamos la instancia del post (producto)
            instancia_producto = form.save(commit=False)
            # Asignamos a la instancia el usuario que este logueado y guardamos
            instancia_producto.creador = request.user
            instancia_producto.save()
            return redirect('producto:crear_producto')

    return HttpResponse("Error al cargar la pagina, intentalo más tarde")


@login_required(login_url='cuenta:inicio_sesion')
def verProducto(request):
    """
    Vista para enlistar todos los productos existentes
    Cuenta con filtro de busqueda y paginacion
    """
    template = 'producto/ver.html'

    # filtramos los resultados para que solo se vean los productos de los cuales el usuario es propietario
    usuario = request.user
    productos = Producto.objects.filter(creador=usuario).order_by('id')

    # Filtrado
    filtro = ProductoFilter(request.GET, queryset=productos)
    productos = filtro.qs

    # Paginación
    # Esto permite instancia paginator y limiatar el resultado a n por pagina
    paginator = Paginator(productos, per_page=50)
    # Esto permite obtener la pagina actual, por defecto es 1
    page_number = request.GET.get('page', 1)
    # page_obj son los objetos dentro de la paginacion actual, aqui estan los elementos que va a pintar la pagina
    # Notar que lo colocamos en el contexto (con .object_list) como productos, ya que solo se van a mostrar estos
    page_obj = paginator.get_page(page_number)

    if request.method == "GET":
        context = {
            "productos": page_obj.object_list,
            'filtro': filtro,
            'paginator': paginator,
            'page_number': int(page_number)
        }
        return render(request, template, context)

    return HttpResponse("Error al cargar la pagina, intentalo más tarde")


@login_required(login_url='cuenta:inicio_sesion')
def editarProducto(request, codigo):
    """
    Vista para editar productos usando como filtro su codigo de barras
    Usamos un try para el caso en donde el codigo del producto no exista
    """
    template = 'producto/editar.html'

    try:
        producto = Producto.objects.get(codigo_barras=codigo)

        if request.method == 'GET':
            form = ProductoForm(instance=producto)
            context = {'formulario': form}
            return render(request, template, context)

        if request.method == 'POST':
            form = ProductoForm(request.POST, instance=producto)
            if form.is_valid():
                form.save()
                return redirect('producto:ver_producto')

    except ObjectDoesNotExist as e:
        error = e
    return render(request, template, {'error': error})


@login_required(login_url='cuenta:inicio_sesion')
def eliminarProducto(request, codigo):
    """
    Vista para eliminar productos usando como filtro su codigo de barras
    Usamos un try para el caso en donde el codigo del producto no exista
    """
    template = "producto/eliminar.html"

    try:

        if request.method == "GET":
            producto = Producto.objects.get(codigo_barras=codigo)
            return render(request, template, {"producto": producto})

        if request.method == "POST":
            if 'eliminar' in request.POST:
                producto = Producto.objects.get(codigo_barras=codigo)
                producto.delete()
            else:
                pass
            return redirect('producto:ver_producto')

    except ObjectDoesNotExist as e:
        error = e
    return render(request, template, {'error': error})
