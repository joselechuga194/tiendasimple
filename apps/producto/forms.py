from django import forms
from .models import *


class ProductoForm(forms.ModelForm):

    nombre = forms.CharField(max_length=200,
                             label="Nombre", required=True, widget=forms.TextInput(attrs={'class': 'campo', 'name': 'nombre', 'autofocus': 'autofocus'}))
    codigo_barras = forms.CharField(
        max_length=500, required=True, widget=forms.NumberInput(attrs={'class': 'campo', 'name': 'codigo_barras'}))
    precio_venta = forms.FloatField(min_value=0.5, widget=forms.NumberInput(
        attrs={'class': 'campo', 'name': 'precio_venta'}))

    class Meta:
        model = Producto
        fields = ['nombre', 'codigo_barras', 'precio_venta']
