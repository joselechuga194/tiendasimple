from django.urls import path
from .views import index, crearProducto, verProducto, editarProducto, eliminarProducto

urlpatterns = [
    path('', index, name="index_producto"),
    path('crear', crearProducto, name="crear_producto"),
    path('ver', verProducto, name="ver_producto"),
    path('editar/<str:codigo>', editarProducto, name="editar_producto"),
    path('eliminar/<str:codigo>', eliminarProducto, name="eliminar_producto")
]
