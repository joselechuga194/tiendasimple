import django_filters
from django import forms
from .models import *


class ProductoFilter(django_filters.FilterSet):
    codigo_barras = django_filters.CharFilter(lookup_expr='iexact', widget=forms.TextInput(
        attrs={'class': 'campo_filtro', 'name': 'codigo_barras',  'placeholder': 'Codigo de barras', 'onfocus': 'oneinput_codigo_barras()'}))
    nombre = django_filters.CharFilter(
        lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'campo_filtro', 'name': 'nombre', 'placeholder': 'Nombre del producto', 'onfocus': 'oneinput_nombre()'}))

    class Meta:
        model = Producto
        fields = '__all__'
        exclude = ['id', 'marca', 'departamento',
                   'descripcion', 'cantidad_inventario', 'fecha_registro', 'precio_compra']
