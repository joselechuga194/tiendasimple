from django.db import models
from datetime import datetime
from django.contrib.auth.models import User


# Create your models here.


class Producto(models.Model):
    """
    Modelo para productos

    Creador: usuario que creo el producto
    Marca: Marca del producto, por defecto es null
    Departamento: Departamento del producto, por defecto es null
    Nombre: Describe el nombre del producto
    descripcion: Describe el producto
    codigo_barras: Codigo unico del producto
    cantidad_inventario: Cantidad en inventario del producto, por defecto es 0
    fecha_registro: Fecha en la que se creo el producto
    precio_compra: Precio por el cual es adquirido el producto (incluye iva)
    precio_venta: Precio por el cual es vendido el producto (incluye iva)

    **Se deja la implemetacion del tamaño del producto, se asume que vendra en el nombre
    """
    id = models.AutoField(primary_key=True)
    creador = models.ForeignKey(
        User, null=True, on_delete=models.SET_NULL, default=None)
    nombre = models.CharField(
        max_length=200, blank=False, null=False)
    codigo_barras = models.CharField(
        max_length=500, null=False, blank=False)
    marca = models.ForeignKey(
        'Marca', default=None, null=True, blank=True, on_delete=models.SET_NULL)
    departamento = models.ForeignKey(
        'Departamento', default=None, null=True, blank=True, on_delete=models.SET_NULL)
    descripcion = models.CharField(max_length=300, blank=True, null=True)
    cantidad_inventario = models.IntegerField(
        default=0, null=False, blank=False)
    fecha_registro = models.DateTimeField(default=datetime.now, blank=True)
    precio_compra = models.FloatField(default=0, null=False, blank=False)
    precio_venta = models.FloatField(null=False, blank=False)

    class Meta:
        db_table = 'producto'
        verbose_name_plural = 'productos'
        unique_together = ('creador', 'codigo_barras')

    def __str__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        if self.nombre:
            raw_name = self.nombre
            self.nombre = raw_name.capitalize()
        super(Producto, self).save(*args, **kwargs)


class Marca(models.Model):
    """
    Modelo para Marcas

    Nombre: Describe el nombre de la marca
    """
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(
        max_length=150, unique=True, null=False, blank=False)

    class Meta:
        db_table = 'marca'
        verbose_name_plural = 'marcas'

    def __str__(self):
        return self.nombre


class Departamento(models.Model):
    """
    Modelo para Departamentos

    Nombre: Describe el nombre del departamento
    """
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(
        max_length=200, unique=True, null=False, blank=False)

    class Meta:
        db_table = 'departamento'
        verbose_name_plural = 'departamentos'

    def __str__(self):
        return self.nombre
