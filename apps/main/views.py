from django.shortcuts import render, redirect

# Create your views here.


def Index(request):
    context = {}
    return render(request, 'main/index.html', context)
