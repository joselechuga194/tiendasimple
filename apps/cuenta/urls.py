from django.urls import path
from .views import registro, inicio_sesion, cerrar_sesion

urlpatterns = [
    path('registro/', registro, name="registro"),
    path('inicio_sesion/', inicio_sesion, name="inicio_sesion"),
    path('cerrar_sesion/', cerrar_sesion, name="cerrar_sesion")
]
