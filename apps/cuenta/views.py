from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .forms import CuentaForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

# Create your views here.


def registro(request):
    """
    Vista para el registro de cuentas
    """
    # Si existe un usuario logeado no le permitimos ver esta vista
    if request.user.is_authenticated:
        return redirect('empresa:index')

    # Usamos el import de cuenta form para hacer el form de creacion
    form = CuentaForm()

    if request.method == 'POST':
        form = CuentaForm(request.POST)
        if form.is_valid():
            usuario = form.save()
            usuario.save()
            messages.success(request, 'Registro exitoso')
            return redirect('cuenta:inicio_sesion')

    context = {'form': form}
    return render(request, 'cuenta/registro.html', context)


def inicio_sesion(request):
    """
    Vista para el inicio de sesión de cuentas
    """
    # Si existe un usuario logeado no le permitimos ver esta vista
    if request.user.is_authenticated:
        return redirect('empresa:index')

    if request.method == "POST":
        # Estos dos toman los inputs del html con estos nombres
        username = request.POST.get('username')

        email = request.POST.get('email')
        password = request.POST.get('password')

        # user_filtered = User.objects.get()

        # usamos el import de authenticate para verificar que el usuario existe
        user = authenticate(
            request, username=username, password=password)

        # si el usuario existe, usamos el import de login para logearlo
        if user is not None:
            login(request, user)
            return redirect('empresa:index')
        else:
            messages.info(request, 'Usuario o contraseña incorrectos')

    context = {}
    return render(request, 'cuenta/inicio_sesion.html', context)


@login_required(login_url='cuenta:inicio_sesion')
def cerrar_sesion(request):
    """
    Vista para el cierre de sesión
    """
    # Usamos el import de logout pasandole el request
    logout(request)
    return redirect('main:index')
