from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Empresa(models.Model):
    """
    Modelo para las empresas

    """
    id = models.AutoField(primary_key=True)
    usuario = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=150, blank=False, null=False)
    # logo = models.ImageField()

    class Meta:
        db_table = 'empresa'
        verbose_name_plural = 'empresas'

    def __str__(self):
        return self.nombre
