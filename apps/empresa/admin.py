from django.contrib import admin
from .models import Empresa


class EmpresaAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)


# Register your models here.
admin.site.register(Empresa, EmpresaAdmin)
