from django.shortcuts import render
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required(login_url='cuenta:inicio_sesion')
def index(request):
    context = {}
    return render(request, 'empresa/base_index.html', context)
