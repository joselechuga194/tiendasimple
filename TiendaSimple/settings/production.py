from .base import *
import os

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['proyectoredes.tk', '172.31.34.117','127.0.0.1']

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': os.environ['DB_NAME_TIENDASIMPLE'],
#         'USER': os.environ['DB_USER'],
#         'PASSWORD': os.environ['DB_PASSWORD_TIENDASIMPLE'],
#         'HOST': os.environ['DB_HOST'],
#         'PORT': 5432
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'tiendasimple',
        'USER': 'administrador',
        'PASSWORD': '1904',
        'HOST': 'db.proyectoredes.tk',
        'PORT': ''
    }
}

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, '../static')
]
